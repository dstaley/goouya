package ouya

import (
	"fmt"
	"testing"
)

func TestGet(t *testing.T) {
	game, _ := Get("com.MattMakesGames.TowerFall")
	if game.Title != "TowerFall" {
		t.Fail()
	}
}

func ExampleGet() {
	game, _ := ouya.Get("com.MattMakesGames.TowerFall")
	fmt.Printf("%+v", game.Title)
	// Output: TowerFall
}

func ExampleGetDownloadLink() {
	url, _ := ouya.GetDownloadLink("com.MattMakesGames.TowerFall")
	fmt.Printf("%+v", url)
	// Output: http://dlyv5j2269q7v.cloudfront.net/.../zKY2ioUkQ2SPbxwSvHwQ_com.MattMakesGames.TowerFall-Aligned.apk
}
