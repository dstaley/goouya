// Package ouya implements simple bindings for the Ouya Discover API.
package ouya

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// A Response contains a response object from the Ouya Discover API.
type Response struct {
	Game  Game          `json:"app,omitempty"`
	Games []GamePartial `json:"apps,omitempty"`
	Error Error         `json:"error,omitempty"`
}

// An Error contains an error code and message from the Ouya Discover API.
type Error struct {
	Code    int    `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

// A GamePartial contains a small amount of information about a game.
//
// GamePartials are returned when requesting /apps from the API.
// Their most important elements are their UUID (which allows you
// to request the full game) and their Tags (which are,
// unfortunately, not returned when requesting a full game).
type GamePartial struct {
	Uuid            string   `json:"uuid,omitempty"`
	Title           string   `json:"title,omitempty"`
	Tags            []string `json:"tags,omitempty"`
	Version         string   `json:"uuid,omitempty"`
	FilepickerImage string   `json:"mainImageFullUrl,omitempty"`
	S3Image         string   `json:"main_image_full_url,omitempty"`
}

// A Game contains all the information about a particular game.
//
// Games are returned when requesting a specific game. However,
// they do not contain the game's tags, which need to be pulled
// seperately from the GamePartial response. (Sucks, I know.)
type Game struct {
	Uuid                  string   `json:"uuid,omitempty"`
	Title                 string   `json:"title,omitempty"`
	Description           string   `json:"description,omitempty"`
	SupportEmail          string   `json:"supportEmailAddress,omitempty"`
	SupportPhone          string   `json:"supportPhone,omitempty"`
	Website               string   `json:"website,omitempty"`
	S3Image               string   `json:"mainImageFullUrl,omitempty"`
	Founder               bool     `json:"founder,omitempty"`
	APKFileSize           int      `json:"apkFileSize,omitempty"`
	PublishedAt           string   `json:"publishedAt,omitempty"`
	Version               string   `json:"versionNumber,omitempty"`
	Likes                 int      `json:"likeCount,omitempty"`
	Overview              string   `json:"overview,omitempty"`
	FilepickerScreenshots []string `json:"filepickerScreenshots,omitempty"`
	ContentRating         string   `json:"contentRating,omitempty"`
	LatestVersion         string   `json:"latestVersion,omitempty"`
	Screenshots           []string `json:"screenshots,omitempty"`
	Developer             string   `json:"developer,omitempty"`
	DownloadLink          string   `json:"downloadLink,omitempty"`
}

// Function GetAndParse gets a specificed URL from the Ouya Discover API,
// unmarshals it into a resonse object, and returns the response object.
func GetAndParse(url string) (resp *Response, err error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("ouya: error getting list of games:", err)
	}
	jsonBlob, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return nil, fmt.Errorf("ouya: error reading list of games:", err)
	}
	err = json.Unmarshal(jsonBlob, &resp)
	if err != nil {
		return nil, fmt.Errorf("ouya: error marshaling list of games:", err)
	}
	return resp, nil
}

// Function GetAll fetches the complete listing of Ouya games, and returns
// them as an array of GamePartials.
func GetAll() ([]GamePartial, error) {
	resp, err := GetAndParse("https://devs.ouya.tv/api/v1/apps")
	if err != nil {
		return nil, err
	}
	if resp.Error.Message != "" {
		return nil, fmt.Errorf("ouya: error returned from the Ouya Discover API:", resp.Error)
	}
	return resp.Games, nil
}

// Function Get fetches the supplied package identifier and returns an instance
// of Game.
func Get(uuid string) (Game, error) {
	resp, err := GetAndParse("https://devs.ouya.tv/api/v1/apps/" + uuid)
	if err != nil {
		return Game{}, err
	}
	if resp.Error.Message != "" {
		return Game{}, fmt.Errorf("ouya: error returned from the Ouya Discover API:", resp.Error)
	}
	return resp.Game, nil
}

// Function GetDownloadLink fecthes and returns the URL of the APK for the
// specified package identifier.
func GetDownloadLink(uuid string) (string, error) {
	resp, err := GetAndParse("https://devs.ouya.tv/api/v1/apps/" + uuid + "/download")
	if err != nil {
		return "", err
	}
	if resp.Error.Message != "" {
		return "", fmt.Errorf("ouya: error returned from the Ouya Discover API:", resp.Error)
	}
	return resp.Game.DownloadLink, nil
}
